extends Position2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var player_cam = get_node("../../Player/Camera2D")
	
	player_cam.limit_right = self.position.x 
	player_cam.limit_top = self.position.y 
	
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
