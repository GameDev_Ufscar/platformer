extends CanvasLayer

signal player_health_changed
var WIN = false

func _ready():
	if(!stageManager.start):
		stageManager.start_timer()
		
	get_child(0).get_child(1).visible = false

func Player_health_changed(HP):
	stageManager.player_hp = HP
	emit_signal("player_health_changed", HP)
	pass

func Player_dead():
	$Interface/GameOver.visible = true
	
func _input(event):
	if event.is_action_pressed("pause") && $Interface/GameOver.visible == false && !WIN:
		stageManager.pause = !stageManager.pause
		get_tree().paused = !get_tree().paused
		$Interface/Pause.visible = !$Interface/Pause.visible
		$Interface/PauseRect.visible = !$Interface/PauseRect.visible