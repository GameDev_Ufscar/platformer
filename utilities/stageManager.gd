extends Node

onready var fader = get_node("screen/controlScreen/anim")
var current_scene
var player_hp = 6
var player_max_hp = 6

var freeze = false
var start = false
var pause = false
var sec_elapsed = 0
var minutes = 0
var str_elapsed

func _input(event):
	if event.is_action_pressed("ui_restart") && !pause && !freeze:
		fader.play("restart-fade-out")

func change_scene(new_scene):
	if !fader.is_playing():
		fader.play("fade-out")
		current_scene = new_scene
	
func _on_anim_animation_finished(anim_name):
	if anim_name == "fade-out":
		get_tree().change_scene(current_scene)
		fader.play("fade-in")
	if anim_name == "restart-fade-out":
		player_hp = player_max_hp
		get_tree().reload_current_scene()
		fader.play("restart-fade-in")
	pass # replace with function body

func _process(delta):
	if(!pause):
		if(start):
			sec_elapsed += 1
			var miliseconds = sec_elapsed % 60
			var seconds = sec_elapsed % 3600 / 60
			str_elapsed = "Time %02d m %02d s %02d ms" % [minutes, seconds, miliseconds]
			#print("delta elapsed : ", str_elapsed)
			if(seconds == 59 && miliseconds == 59):
				minutes += 1

func start_timer():
	start = true
	set_process(true)
	
func freeze():
	set_process(false)
	freeze = true
