extends Control

var pressed = false
export(String, FILE, "*.tscn") var world_scene


func _input(event):
	if event is InputEventKey and pressed == false:
		if event.pressed:
			stageManager.change_scene("res://levels/Tutorial/Tutorial.tscn")
			pressed = true
