extends Area2D

var cooldown = 0
var freeze = false

onready var hud = get_node("../HUD")
onready var player_cam = get_node("../Player/Camera2D")

func show_results():
	set_process(true)


func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			if !freeze:
				$WIN.visible = true
				print("entered!")
				body.state = 0
				stageManager.freeze()
				show_results()
				hud.WIN = true
				player_cam.align()
				$TimerResult.text = stageManager.str_elapsed
			freeze = true
			#get_tree().change_scene(world_scene)

func _process(delta):
	if(freeze):
		cooldown += 1
	if cooldown == 60:
		$TimerResult.visible = true