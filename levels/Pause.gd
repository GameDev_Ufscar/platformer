extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = !get_tree().paused
		