extends MarginContainer

onready var bar = $HBoxContainer/Bars/Bar/Gauge
var node
var Player

func _ready():
	update_bar();
	
func _on_HUD_player_health_changed(HP):
	if(HP <= 0): bar.value = 0

	bar.value = HP
	#print(bar.value)
	pass # replace with function body

func update_bar():
	if bar == null:
		return
	bar.max_value = stageManager.player_max_hp
	bar.value = stageManager.player_hp

#func _on_Camera2D_camera_pos(pos):
	#var a = pos + Vector2(-476, -282)
	#bar.set_global_position(a)
	#tween.interpolate_property(bar,"position", pos, a, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)

