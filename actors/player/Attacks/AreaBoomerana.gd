extends Area2D

# Ataques: Quando o ataque em si detecta algum corpo que ele entrou, ele executa sua logica

var dir
var player
var MAXSPEED
var node
var SPEED
var MOTION = Vector2(0,0)
export (int) var DAMAGE

func _ready():
	for i in range(0, 99):
		node = get_parent().get_child(i)
		if node.name == "Player":
			player = node
			break
	dir = player.shoot_dir
	MAXSPEED = 1000
	SPEED = 1000 + abs(player.SPEED)
	pass

func _process(delta):
	
	### Movement Behavior ###
	
	SPEED = approach(SPEED, MAXSPEED*(-1), 50)
	
	MOTION.x = SPEED*dir
	MOTION.y = 0
	self.position = (position +  MOTION * delta)
	
	### Animation ###
	
	if dir == 1:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true
	
	### Damage Calculation ###
	if dir == 1:
		if MOTION.x == -MAXSPEED:
			DAMAGE = 2
	if dir == -1:
		if MOTION.x == MAXSPEED:
			DAMAGE = 2

	pass


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	pass # replace with function body

func approach(a, b, amount):
	
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a
	
	pass


func _on_AreaBoomerana_body_entered(body):
	if body.name == "Player" or body.name == "TileMap":
		queue_free()
	
	if body.has_method("hit_by_bullet"):
		body.HP -= DAMAGE
		body.call("hit_by_bullet")
		queue_free()
	pass # replace with function body
