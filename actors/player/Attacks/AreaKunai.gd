extends Area2D

var dir
var player
var node
var SPEED
var MOTION = Vector2(0,0)
export (int) var DAMAGE

func _ready():
	for i in range(0, 99):
		node = get_parent().get_child(i)
		if node.name == "Player":
			player = node
			break
	dir = player.shoot_dir
	SPEED = 1000 + abs(player.SPEED)

func _process(delta):
	### Collision ###
	
	#var bodies = get_overlapping_bodies()
	#for body in bodies:
	#	if body.name == "Player" or body.name == "TileMap":
	#		queue_free()
	
	### Movement Behavior ###
	
	MOTION.x = SPEED*dir
	MOTION.y = 0
	self.position = (position +  MOTION * delta)
	
	### Animation ###
	
	if dir == 1:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true
		
	pass

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	pass # replace with function body


func approach(a, b, amount):
	
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a
	
	pass
	


func _on_AreaKunai_body_entered(body):
	if body.name == "Player" or body.name == "TileMap":
		queue_free()
	
	if body.has_method("hit_by_bullet"):
		body.HP -= DAMAGE
		body.call("hit_by_bullet")
		queue_free()
	pass # replace with function body
