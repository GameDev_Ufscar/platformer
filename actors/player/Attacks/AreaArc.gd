extends Area2D

var dir
var player
var SPEED
var GRAV = 60
var MOTION = Vector2(0,0)
export (int) var DAMAGE

func _ready():
	player = get_parent().get_child(0)
	dir = player.shoot_dir
	SPEED = 400 + abs(player.SPEED)
	MOTION.y = -1000

func _process(delta):
	
	### Movement Behavior ###
	MOTION.x = SPEED*dir
	MOTION.y = min(MOTION.y + GRAV, 1200)
	self.position = (position +  MOTION * delta)
	
	### Animation ###
	
	if dir == 1:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true
	pass

func approach(a, b, amount):
	
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a
	
	pass
	

func _on_AreaArc_body_entered(body):
	if body.name == "Player":
		queue_free()
	
	if body.has_method("hit_by_bullet"):
		body.HP -= DAMAGE
		body.call("hit_by_bullet")
		queue_free()
	pass # replace with function body



func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	pass # replace with function body
