 extends KinematicBody2D

var ACC = 60
var MAXSPEED = 375
var JUMP_HEIGHT = -500

var motion = Vector2()
var jumped = false			### boolean para walljump
const GRAV = 30
var SPEED = 0

const IMPULSE = -20

var KNOCKBACK = 0
var k_dir = 0

var jump_cooldown = 0
var UP = Vector2(0,-1)
var jump_fair_buffer = 0 	### fazer o pulo um pouco mais facil ao andar fora de uma plataforma
var walljump_fair_buffer = 0
var dir = 1
var shoot_dir				### definir de onde o projetil sai do jogador
var has_control = true
var BLINK = false
var attack_string = "Boomerana"
var COGUMELO_ESTRAGADO = false

onready var hud = get_node("../HUD")
var anim = "idle"

var HP

# Player States
enum PLAYER {
	WIN,
	IDLE,
	MOVE,
	JUMP,
	FALL,
	GLIDE,
	WALLJUMP,
	DAMAGE,
	DEAD
}

var state = PLAYER.IDLE

func _ready():
	HP = stageManager.player_hp
	hud.Player_health_changed(HP)

func _physics_process(delta):
	
	var new_anim
	
	new_anim = "idle"
	
	if COGUMELO_ESTRAGADO == true:
		self.set_scale(get_scale() * 1.01)
	
	### Controls ###

	var key_right = 0
	var key_left = 0
	var key_jump = 0
	#var key_dash = Input.is_action_pressed("dash")
	var key_shoot = 0
	var key_glide = 0
	
	if UP.y == 1:
		motion.y = min(motion.y + GRAV, -600)
		JUMP_HEIGHT = 500
	else:
		motion.y = min(motion.y + GRAV, 600)
	
	if has_control:
		if Input.is_action_pressed("ui_right"):
			key_right = 1

		if Input.is_action_pressed("ui_left"):
			key_left = -1
	
		if Input.is_action_just_pressed("jump"):
			key_jump = 1
		if Input.is_action_pressed("jump"):
			key_glide = 1
			
		if Input.is_action_just_pressed("shoot"):
			key_shoot = 1
	
	jump_fair_buffer -= 1
	walljump_fair_buffer -= 1
	
	dir = (key_left + key_right)
	
	### State Machine ###
	
	match state:
		PLAYER.WIN:
			new_anim = "win"
			has_control = false
			SPEED = 0
		PLAYER.IDLE:
			### Animation ###
			new_anim = "idle"
			
			### Movement ###
			
			SPEED = approach(SPEED, MAXSPEED*dir, ACC)
			
			### State transition ###
			
			if SPEED != 0 && !is_on_wall():
				state = PLAYER.MOVE
			
			if !is_on_floor():
				state = PLAYER.FALL
			
			if is_on_floor():
				if key_jump:
					jump()
					
		PLAYER.MOVE:
			
			#if key_dash:
			#	$Sprite.play("dash")
			#	MAXSPEED = 500
			#else:
			new_anim = "run"
			
			SPEED = approach(SPEED, MAXSPEED*dir, ACC)
			
			if is_on_wall() or SPEED == 0:
				state = PLAYER.IDLE
			
			if !is_on_floor():
				jump_fair_buffer = 5
				state = PLAYER.FALL
			
			if is_on_floor():
				if key_jump:
					jump()
		
		PLAYER.JUMP:
			
			### Animation ###
			new_anim = "jump"
			
			SPEED = approach(SPEED, MAXSPEED*dir, 50)
			
			if Input.is_action_pressed("jump") and jump_cooldown > 0:
				motion.y += IMPULSE
			
			jump_cooldown -= 1 ### Quando não decrementado, deixa o pulo mais flutuante
			
			if motion.y >= -100:
				state = PLAYER.FALL
				
		PLAYER.FALL:
				
			new_anim = "fall"
			
			SPEED = approach(SPEED, MAXSPEED*dir, 50)
			
			if is_on_floor():
				if motion.x == 0:
					state = PLAYER.IDLE
				else:
					state = PLAYER.MOVE
			
			### making jump easier ###
			if jump_fair_buffer > 0 && key_jump:                   
				jump()
			
			if is_on_wall() && (SPEED > 0 || SPEED < 0) && (key_left*(-1) or key_right):
				state = PLAYER.WALLJUMP
			
			if (motion.y > -100) && Input.is_action_just_pressed("jump") and has_control:
				state = PLAYER.GLIDE
			
		PLAYER.WALLJUMP:
			#if is_on_floor():
			#	state = PLAYER.IDLE
			
			if jumped:
				new_anim = "jump"
				
				SPEED = approach(SPEED, (MAXSPEED + 150)*dir, 10)
				
				#if key_jump && jump_cooldown > 0:
				#	motion.y += IMPULSE
			
				#jump_cooldown = jump_cooldown - 1 
				
				if is_on_wall():
					state = PLAYER.WALLJUMP
					jumped = false
				
				if motion.y >= 0:
						state = PLAYER.FALL

			if Input.is_action_just_pressed("jump") && !jumped:
				if SPEED >= 0:
					SPEED = (MAXSPEED + 150) * (-1)
				else:
					SPEED = (MAXSPEED + 150)
					
				motion.y = JUMP_HEIGHT - 200
				jumped = true
				
			elif !jumped:
				new_anim = "walljump"
				
				if !is_on_wall():
					state = PLAYER.FALL
				
				if !(key_left*(-1)) && !key_right:
					if walljump_fair_buffer == 0:
						state = PLAYER.FALL
				else:
					walljump_fair_buffer = 5
				
				motion.y = min(motion.y + (GRAV), 200)
				
		PLAYER.GLIDE:
			new_anim = "glide"
			motion.y = 150
				
			SPEED = approach(SPEED, MAXSPEED*dir, 10)
				
			if is_on_floor():
				if motion.x == 0:
					state = PLAYER.IDLE
				else:
					state = PLAYER.MOVE
			if !key_glide:
				state = PLAYER.FALL
			
			if is_on_wall() && (SPEED > 0 || SPEED < 0) && (key_left*(-1) or key_right):
				state = PLAYER.WALLJUMP
		
		PLAYER.DAMAGE:
			new_anim = "damage"
			has_control = false
			
			KNOCKBACK = approach(KNOCKBACK, 0, 25)
			
			SPEED = KNOCKBACK
			
			if SPEED == 0:
				has_control = true
				KNOCKBACK = 500
				if is_on_floor():
					state = PLAYER.IDLE
				else:
					state = PLAYER.FALL
			else:
				state = PLAYER.DAMAGE
			
		PLAYER.DEAD:
			SPEED = 0
			new_anim = "dead"
			$CollisionShape2D.disabled = true
			has_control = false
			BLINK = false
	
	if BLINK:
		blink()
	else:
		BLINK = false
		visible = true
	
	motion.x = SPEED
	jump_cooldown = max(0, jump_cooldown)
	jump_fair_buffer = max(0, jump_fair_buffer)
	walljump_fair_buffer = max(0, walljump_fair_buffer)
	
	motion = move_and_slide(motion,UP)
	
	if(motion.x > 0):
		$Sprite.flip_h = false
	
	if(motion.x < 0):
		$Sprite.flip_h = true
	
	
	### Shooting ###
	if key_shoot:
		new_anim = "attack"
		if $Sprite.flip_h == false:
			shoot_dir = 1
		else:
			shoot_dir = -1
		$Spawner.fire(shoot_dir, attack_string)

	if new_anim != anim:
		anim = new_anim
		$anim.play(anim)
		
	pass


func approach(a, b, amount):
	
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a
	
	pass

func jump():
	motion.y = JUMP_HEIGHT
	jump_cooldown = 32
	state = PLAYER.JUMP

func damage(d, K_IN):
	if d == 999 and HP > 3:
		HP = 1
	else:
		HP -= d
	hud.Player_health_changed(HP)
	if HP <= 0:
		dead()
	else:
		KNOCKBACK = 575*(K_IN)
		set_collision_mask_bit(2, false)
		set_collision_layer_bit(1, false)
		$iframes.start()
		BLINK = true
		motion.y = -300
		state = PLAYER.DAMAGE
		#get_collision_mask_bit()

func dead():
	state = PLAYER.DEAD
	motion.y = -700
	hud.Player_dead()
	$Camera2D.current = false

func blink():
	visible = !visible

func _on_iframes_timeout():
	set_collision_mask_bit(2, true)
	set_collision_layer_bit(1, true)
	BLINK = false
