extends Node2D

func fire(shoot_dir, attack):
	
	var bullet = preload("res://actors/player/Attacks/AreaBoomerana.tscn").instance()
	
	if not $CooldownBullet.is_stopped():
		return
	
	$CooldownBullet.start()
	if shoot_dir == 1:
		bullet.position = $bullet_shoot_right.global_position
	else:
		bullet.position = $bullet_shoot_left.global_position
	get_parent().get_parent().add_child(bullet)
	
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
