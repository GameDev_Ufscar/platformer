extends KinematicBody2D

# A CollisionShape do node serve para detectar paredes para movimentacao, enquanto a
# hitbox detecta o ataque para realizar no Player ou algum ataque de bullet ou stomp


var SPEED = 100
var GRAV = 20
var MOTION = Vector2(0,0)
var UP = Vector2(0,-1)
var dir = 1
var HP = 3
var DMG = 1

onready var detect_floor_left = $dec_floor_l
onready var detect_wall_left = $dec_wall_l
onready var detect_floor_right = $dec_floor_r
onready var detect_wall_right = $dec_wall_r
onready var sprite = $Sprite

func _ready():
	$Sprites.play("walk")
	#detect_floor_left.add_exception(get_node("hitbox"))
	#detect_wall_left.add_exception(get_node("hitbox"))
	#detect_floor_right.add_exception(get_node("hitbox"))
	#detect_wall_right.add_exception(get_node("hitbox"))

func _physics_process(delta):
		
	for body in get_node("hitbox").get_overlapping_bodies():
		if body.name == "Player":
			if body.motion.y > 0:
				body.jump()
				stomped()
			else:
				body.damage(DMG, dir)
	
	### Movement ###
	MOTION.x = SPEED*dir
	MOTION.y = min(MOTION.y + GRAV, 1200)
	
	MOTION = move_and_slide(MOTION, UP)
	
	### Wall Collision ###
	if not detect_floor_left.is_colliding() or detect_wall_left.is_colliding():
		$Sprite.flip_h = true
		dir = 1
		$Sprites.play("walk1")

	if not detect_floor_right.is_colliding() or detect_wall_right.is_colliding():
		$Sprite.flip_h = false
		dir = -1
		$Sprites.play("walk")

func stomped():
	$Sprites.play("die")
	queue_free()

func hit_by_bullet():
	
	if HP <= 0:
		queue_free()
		
