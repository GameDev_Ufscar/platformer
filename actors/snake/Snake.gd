extends KinematicBody2D

var SPEED = 150
var GRAV = 20
var MOTION = Vector2(0,0)
var UP = Vector2(0,-1)
var dir = 1
var HP = 3
var DMG = 1
var anim = ""
var new_anim = ""

onready var detect_floor_left = $dec_floor_l
onready var detect_wall_left = $dec_wall_l
onready var detect_floor_right = $dec_floor_r
onready var detect_wall_right = $dec_wall_r
onready var attack_player = $attack_player
onready var sprite = $Sprite

#func _ready():
	
	### Exceptions ###
	#detect_floor_left.add_exception(get_node("hitbox"))
	#detect_wall_left.add_exception(get_node("hitbox"))
	#detect_floor_right.add_exception(get_node("hitbox"))
	#detect_wall_right.add_exception(get_node("hitbox"))
	#detect_floor_left.add_exception(get_node("attack_hitbox_point/attack_hitbox"))
	#detect_wall_left.add_exception(get_node("attack_hitbox_point/attack_hitbox"))
	#detect_floor_right.add_exception(get_node("attack_hitbox_point/attack_hitbox"))
	#detect_wall_right.add_exception(get_node("attack_hitbox_point/attack_hitbox"))

func _physics_process(delta):
	
	var new_anim = "idle"
	
	for body in get_node("hitbox").get_overlapping_bodies():
		if body.name == "Player":
			if body.motion.y > 0:
				body.jump()
				stomped()
			else:
				body.damage(DMG, dir)
	
	### Movement ###
	if not $anim.current_animation == "attack":
		MOTION.x = SPEED*dir
		MOTION.y = min(MOTION.y + GRAV, 1200)
	else:
		MOTION.x = 0
	
	MOTION = move_and_slide(MOTION, UP)
	
	### Wall Collision ###
	if not detect_floor_left.is_colliding() or detect_wall_left.is_colliding():
		$Sprite.flip_h = true
		$attack_hitbox_point.position.x = 24 
		attack_player.cast_to.x = 30
		dir = 1

	if not detect_floor_right.is_colliding() or detect_wall_right.is_colliding():
		$Sprite.flip_h = false
		$attack_hitbox_point.position.x = -24
		attack_player.cast_to.x = -30
		dir = -1
	
	### Attacks ###
	if attack_player.is_colliding():
		attack()
		new_anim = "attack"
	
	if new_anim != anim:
		anim = new_anim
		$anim.play(anim)

func attack(): 
	for body in get_node("attack_hitbox_point").get_node("attack_hitbox").get_overlapping_bodies():
		if body.name == "Player":
			body.damage(DMG+1, dir)

func stomped():
	queue_free()

func hit_by_bullet():
	$anim.play("damage")
	if HP <= 0:
		queue_free()